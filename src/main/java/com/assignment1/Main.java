package com.assignment1;

public class Main {

    public static void main(String[] args) {
	    Calculator calculator = new Calculator();
        System.out.println("-----------------------------------");
        System.out.println("Running the calculator application!");
        System.out.println("-----------------------------------");
        System.out.println("3 + 4 + 5 = " + calculator.add("3;4;5"));
        System.out.println("-----------------------------------");
    }
}
