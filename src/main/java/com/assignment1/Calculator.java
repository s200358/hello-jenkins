package com.assignment1;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Calculator {
    public Calculator() {
    }

    public int add(String numbers) {
        numbers = numbers.replace("\\n","\n");

        String delimiter = ";";
        List<Integer> negatives = new ArrayList<>();
        if (numbers == null || numbers.isEmpty()) {
            return 0;
        }
        if (numbers.contains("\n")) {
            delimiter = numbers.substring(2, numbers.indexOf("\n"));
            numbers = numbers.substring(numbers.indexOf("\n") + 1);
        }
        int sum = 0;
        int delimIndex = numbers.indexOf(delimiter);
        if (delimIndex == -1) {
            return Integer.parseInt(numbers);
        }
        while (numbers.length() > 0) {
            if (delimIndex == -1) {
                delimIndex = numbers.length();
            }
            int parsed = Integer.parseInt(numbers.substring(0, delimIndex));
            if (parsed < 0) {
                negatives.add(parsed);
            }
            sum += parsed;
            if (delimIndex + delimiter.length() > numbers.length()) {
                numbers = "";
            } else {
                numbers = numbers.substring(delimIndex + delimiter.length());
                delimIndex = numbers.indexOf(delimiter);
            }
        }
        if (!negatives.isEmpty()) {
            throw new IllegalArgumentException("Negatives not allowed: " + negatives.stream().map(Object::toString).collect(Collectors.joining()));
        }
        return sum;
    }
}
