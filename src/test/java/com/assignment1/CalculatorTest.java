package com.assignment1;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.junit.platform.engine.Cucumber;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    Calculator calculator;
    String parameter;
    public CalculatorTest() {
    }

    @Given("^I have a calculator that only takes a single string")
    public void iHaveACalculatorThatOnlyTakesASingleString() throws Exception {
        this.calculator = new Calculator();
    }

    @When("^I pass a string containing 3 numbers, \"([^\"]*)\", to the calculator$")
    public void iPass3Numbers(String parameters) {
        this.parameter = parameters;
    }

    @Then("^the calculator returns the sum of the 3 provided numbers, which is \"([^\"]*)\"$")
    public void theCalculatorReturnsTheSumOf3Numbers(int sum) {
        assertEquals(sum, calculator.add(parameter));
    }

    @When("^I pass a string containing multiple numbers, \"([^\"]*)\", to the calculator$")
    public void IPassMultipleNumbers(String parameters) {
        this.parameter = parameters;
    }

    @Then("^the calculator returns the sum of the provided numbers, which is \"([^\"]*)\"$")
    public void theCalculatorReturnsTheSumOfMultipleNumbers(int sum) {
        assertEquals(sum, calculator.add(parameter));
    }

    @When("^I pass a string containing multiple numbers with a custom delimiter, \"([^\"]*)\", to the calculator$")
    public void IPassMultipleNumbersWithACustomDelimiter(String parameters) {
        this.parameter = parameters;
    }

    @Then("^the calculator returns the sum of the provided numbers, when providing a custom delimiter, which is \"([^\"]*)\"$")
    public void theCalculatorReturnsTheSumOfMultipleNumbersWithACustomDelimiter(int sum) {
        assertEquals(sum, calculator.add(parameter));
    }

    @When("^I pass a string containing some negative numbers, \"([^\"]*)\", to the calculator$")
    public void IPassMultipleNumbersWithNegatives(String parameters) {
        this.parameter = parameters;
    }

    @Then("^the calculator returns an error, \"([^\"]*)\"$")
    public void theCalculatorThrowsAnError(String error) {
        try {
            calculator.add("//,\n1,-2,-3");
        } catch (IllegalArgumentException e) {
            assertEquals("Negatives not allowed: -2-3", e.getMessage());
        }
    }
}