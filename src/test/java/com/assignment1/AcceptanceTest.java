package com.assignment1;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/use_cases",
        monochrome=true,
        snippets = SnippetType.CAMELCASE,
        glue = { "com.assignment1"})
public class AcceptanceTest {

}
