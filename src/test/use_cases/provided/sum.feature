Feature: Sum
	Description: A calculation is performed
	Actors: User

Scenario: Calculate sum of 3 numbers from a string
 	Given I have a calculator that only takes a single string
 	When I pass a string containing 3 numbers, "1;2;3", to the calculator
 	Then the calculator returns the sum of the 3 provided numbers, which is "6"
 	
Scenario: Calculate sum of multiple numbers from a string
	Given I have a calculator that only takes a single string
 	When I pass a string containing multiple numbers, "1;2;3;4;5", to the calculator
	Then the calculator returns the sum of the provided numbers, which is "15"

Scenario: Calculate sum of multiple numbers from a string with a custom delimiter
	Given I have a calculator that only takes a single string
	When I pass a string containing multiple numbers with a custom delimiter, "//,\n1,2,3", to the calculator
	Then the calculator returns the sum of the provided numbers, when providing a custom delimiter, which is "6"

Scenario: Throw an exception when the numbers contain a negative number
	Given I have a calculator that only takes a single string
	When I pass a string containing some negative numbers, "-2;-3;2", to the calculator
	Then the calculator returns an error, "Negatives not allowed: -2-3"